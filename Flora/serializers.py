from rest_framework import serializers
from Flora.models import *
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_auth.serializers import UserDetailsSerializer
from rest_framework import request


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile

        fields = ('id', 'zip_code', 'city', 'hardiness_zone')
        read_only_fields = ('hardiness_zone',)


class PlantSerializer(serializers.ModelSerializer):

    class Meta:
        model = Plant

        fields = (
        'id', 'scientific', 'common', 'description', 'hardiness', 'space', 'toxicity', 'photonFluxDensity', 'grow_time',
        'water',
        'min_temp', 'max_temp', 'ripeURL')


class GardenBedSerializer(serializers.ModelSerializer):
    class Meta:
        model = GardenBed

        fields = ('id', 'owner', 'name', 'plants', 'calendar')
        read_only_fields = ('calendar', 'owner')


class CalendarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Calendar

        fields = ('id', 'garden_bed', 'date_created', 'date_last_watered')


        # class HardinessSerializer(serializers.ModelSerializer):
        # class Meta:
        # model = HardinessZones

        #fields = ('__all__')
