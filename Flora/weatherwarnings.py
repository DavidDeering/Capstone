from __future__ import print_function
import smtplib

from Flora.models import *
from django.contrib.auth.models import User
import unirest, datetime



class WeatherWarning:
    def __init__(self):
        self.users = User.objects.all()
        self.gardenbeds = GardenBed.objects.all()
        self.calendars = Calendar.objects.all()
        self.userprofiles = UserProfile.objects.all()
        self.plants = Plant.objects.all()
        self.city_set = self.get_cities()

    def get_cities(self):
        city_set = set()
        for user in self.users:
            profile = self.userprofiles.get(id=user.id)
            city = profile.city
            city_set.add(city)
            return city_set

    def send_email(self, sender, pwd, recipient, subject, body):

        TO = recipient if type(recipient) is list else [recipient]

        # Prepare actual message
        message = """From: %s\nTo: %s\nSubject: %s\n\n%s
        """ % (sender, ", ".join(TO), subject, body)
        try:
            server = smtplib.SMTP("smtp.gmail.com", 587)
            server.ehlo()
            server.starttls()
            server.login(sender, pwd)
            server.sendmail(sender, TO, message)
            server.close()
        except:
            # need to turn this into saving to a log file or something
            print("failed to send mail")

    def analyze_weather(self):

        def callback_function(response):
            i = 0
            max_temp = []
            min_temp = []
            rain = False

            while i < 8:
                max_temp.append(response.body['list'][i]['main']['temp_max'])
                min_temp.append(response.body['list'][i]['main']['temp_min'])

                if 'rain' in response.body['list'][i]:
                    rain = True
                i += 1

            for user in self.users:
                gb = GardenBed.objects.filter(owner=user)
                plants_in_danger = set()
                # freezing = False
                # danger = False
                # condition = 'hot'

                for bed in gb:
                    calendar = Calendar.objects.get(id=bed.id)
                    if rain:
                        calendar.date_last_watered = datetime.date.today()
                        calendar.save()

                    for plant in bed.plants.all():
                        i = 0
                        for mintemp, maxtemp in zip(min_temp, max_temp):
                            if plant.min_temp > mintemp:
                                plants_in_danger.add(plant.common)
                                #freezing = False
                                danger = True
                            elif plant.max_temp < maxtemp:
                                plants_in_danger.add(plant.common)
                                #freezing = True
                                danger = True

                                # if freezing:
                                #condition = 'cold'

                if danger:
                    sender = ''
                    pwd = ''
                    subject = 'Flora App Weather Warning!'
                    recipient = user.email
                    message = 'This is a warning from FLORA.  The weather forecast in your area predicts ' \
                              'that the temperature will be dangerous for the following plants:\n'
                    for plant in plants_in_danger:
                        message = message + ' ' + plant + '\n '

                    self.send_email(sender, pwd, recipient, subject, message)

        for city in self.city_set:
            key = '46a933670f9d4dfdd006a284fbe2ae51'
            url = 'http://api.openweathermap.org/data/2.5/forecast?q=' + city + ',US&units=imperial&APPID=' + key
            unirest.get(url, headers={}, params={}, callback=callback_function)
