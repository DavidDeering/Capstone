from django.shortcuts import render
from rest_framework import viewsets
from Flora.models import *
from Flora.serializers import *
from rest_framework.authentication import BasicAuthentication, SessionAuthentication, TokenAuthentication
from rest_framework import permissions, generics
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth import get_user_model
from django.core import serializers
from django.contrib.auth.models import User
import unirest
import xml.etree.ElementTree as ET
from rest_framework.response import Response
from rest_framework import filters



# Create your views here.
class PlantViewSet(viewsets.ReadOnlyModelViewSet):
    # authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication,)
    # permission_classes = permissions.IsAuthenticated

    queryset = Plant.objects.all()
    serializer_class = PlantSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('hardiness',)


class GardenBedViewSet(viewsets.ModelViewSet):
    serializer_class = GardenBedSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user
        if user is not None:
            return GardenBed.objects.filter(owner=user)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class CalendarViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Calendar.objects.all()
    serializer_class = CalendarSerializer

    filter_fields = ('garden_bed',)


class UserProfileViewSet(viewsets.ModelViewSet):
    serializer_class = UserProfileSerializer

    def get_queryset(self):
        user = self.request.user
        if user is not None:
            return UserProfile.objects.filter(user=user)

    def perform_create(self, serializer):
        zip_code = self.request.data.get('zip_code', None)
        # use synch call to api to set hardiness zone
        response = unirest.post('http://www.plantmaps.com/pm_queries.php?&Z2Z=' + zip_code + '&_= HTTP/1.1')
        resp_file = response.body
        splitted = str.split(resp_file)
        serializer.save(user=self.request.user, hardiness_zone=splitted[10][0])

