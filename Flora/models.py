from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.core.validators import validate_comma_separated_integer_list
from django.db import models
from django.core.exceptions import ValidationError



# Create your models here.
# class HardinessZones(models.Model):
# zone_number = models.PositiveIntegerField()

# def __unicode__(self):
# return '%s' % (self.zone_number)


class Plant(models.Model):
    hardiness = models.CharField(max_length=20, validators=[validate_comma_separated_integer_list])
    photonFluxDensity = models.TextField()
    space = models.TextField()
    toxicity = models.TextField()
    grow_time = models.PositiveIntegerField()
    scientific = models.TextField()
    common = models.TextField()
    ripeURL = models.URLField()
    description = models.TextField()
    water = models.PositiveIntegerField()
    min_temp = models.IntegerField()
    max_temp = models.IntegerField()

    def __unicode__(self):
        return '%d' % (self.id)


class GardenBed(models.Model):
    name = models.TextField()
    plants = models.ManyToManyField(Plant)
    owner = models.ForeignKey('auth.user', related_name='gardenbed')

    def __unicode__(self):
        return '%d' % (self.id)


class Calendar(models.Model):
    date_created = models.DateField()
    date_last_watered = models.DateField()
    garden_bed = models.OneToOneField(GardenBed)

    def __unicode__(self):
        return '%d' % (self.id)


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    zip_code = models.CharField(max_length=5)
    hardiness_zone = models.SmallIntegerField()
    city = models.CharField(max_length=21)

    def __unicode__(self):
        return '%d' % (self.id)
