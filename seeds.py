# class Plant(models.Model):
#    hardiness = models.ManyToManyField(HardinessZones, related_name='hardiness')
#    photonFluxDensity = models.TextField()
#    space = models.TextField()
#    grow_time = models.PositiveIntegerField()
#    scientific = models.TextField()
#    common = models.TextField()
#    ripeURL = models.URLField()
#    description = models.TextField()
#    water = models.PositiveIntegerField()
#    min_temp = models.IntegerField()
#    max_temp = models.IntegerField()

from django.db import models
from Flora.models import *


# plant1 = Plant.objects.create(hardiness=[4,5,6,7], photonFluxDensity='High', space='low', toxicity='none',grow_time=42, scientific='Fakem Namus Demonsratium', common='Demo Plant',ripeURL='http://fakeurl.com', description='This plant will demonstate the email warning system',water = 2, min_temp=95,max_temp=96)
Plant.objects.create(hardiness=[4, 5, 6, 7, 8, 9], photonFluxDensity='Full Sun',
                     space='6 inches wide, 15-18 inches apart', toxicity='Repels deer', grow_time=712,
                     scientific='Asparagus officinalis', common='Asparagus',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/image_nodes/asparagus-plant-grow-harvest.jpg?itok=3p_-iGxE',
                     description='Asparagus is a perennial bulb and stem vegetable.  Takes 2-3 years to start but will produce vegetables for up to 20 years',
                     water=4, min_temp=32, max_temp=85)
Plant.objects.create(hardiness=[3, 4, 5, 6, 7, 8, 9, 10], photonFluxDensity='Full Sun', space='2 inches apart',
                     toxicity='None', grow_time=55, scientific='Phaseolus vulgaris', common='Green Pole Bean',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/greenbeans.jpg?itok=dGNLktjf',
                     description='Green beans, also known as French beans, string beans, or snap beans, are the unripe fruit and protective pods of various cultivars of the common bean ',
                     water=1, min_temp=60, max_temp=85)
Plant.objects.create(hardiness=[2, 3, 4, 5, 6, 7, 8, 9, 10], photonFluxDensity='Full Sun', space='1-2 inches apart',
                     toxicity='None', grow_time=50, scientific='Beta vulgaris', common='Beet',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/beets.jpg?itok=dTkwZf3U',
                     description='One of several of the cultivated varieties of Beta vulgaris grown for their edible taproots and their leaves (called beet greens)',
                     water=1, min_temp=30, max_temp=90)
Plant.objects.create(hardiness=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], photonFluxDensity='Full Sun',
                     space='2 together, 2 inches between pairs', toxicity='None', grow_time=110,
                     scientific='Capsicum annuum', common='Bell Pepper',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/yellow_bell_peps.jpg?itok=dpTrTMBl',
                     description='Cultivars of the plant produce fruits in different colors, including red, yellow, orange, green, chocolate/brown, vanilla/white, and purple',
                     water=7, min_temp=60, max_temp=90)
Plant.objects.create(hardiness=[3, 4, 5, 6, 7, 8, 9, 10], photonFluxDensity='Full Sun',
                     space='12-24 inches apart, 36 inches between rows', toxicity='None', grow_time=85,
                     scientific='Brassica oleracea', common='Broccoli',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/broccoli.jpg?itok=d58jVA8j',
                     description='Kids love broccoli!', water=2, min_temp=40, max_temp=90)
Plant.objects.create(hardiness=[3, 4, 5, 6, 7, 8, 9, 10], photonFluxDensity='Full Sun',
                     space='3 inches apart, Needs a trellis', toxicity='None', grow_time=55,
                     scientific='Phaseolus vulgaris', common='Green Bush Bean',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/greenbeans.jpg?itok=dGNLktjf',
                     description='Green beans, also known as French beans, string beans, or snap beans, are the unripe fruit and protective pods of various cultivars of the common bean ',
                     water=1, min_temp=70, max_temp=85)
Plant.objects.create(hardiness=[2, 3, 4, 5, 6, 7, 8, 9], photonFluxDensity='Full Sun',
                     space='12 to 24 inches apart', toxicity='None', grow_time=90,
                     scientific='Brassica oleracea var. gemmifera', common='Brussels Sprouts',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/brusselssprouts.jpg?itok=xGL9SA2W',
                     description='Brussels Sprouts are a member of the cabbage family, and an excellent source of protein and vitamins',
                     water=3, min_temp=40, max_temp=85)
Plant.objects.create(hardiness=[1, 2, 3, 4, 5, 6, 7, 8, 9], photonFluxDensity='Full Sun',
                     space='12-24 inches', toxicity='Do not plant near strawberries', grow_time=70,
                     scientific='Brassica oleracea var. capitata', common='Cabbage',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/Cabbage.jpg?itok=Zp9UDSWE',
                     description='Cabbage is a hardy, leafy vegetable full of vitamins.',
                     water=2, min_temp=39, max_temp=75)
Plant.objects.create(hardiness=[4, 5, 6, 7, 8, 9, 10], photonFluxDensity='Full Sun',
                     space='3-4 inches', toxicity='None', grow_time=75,
                     scientific='Daucus carota', common=' Carrot',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/carrots.jpg?itok=OmiLJ7Ow',
                     description='A root vegetable, usually orange in color, though purple, black, red, white, and yellow varieties exist',
                     water=7, min_temp=30, max_temp=90)
Plant.objects.create(hardiness=[], photonFluxDensity='Full Sun',
                     space='18-24 inches', toxicity='None', grow_time=75,
                     scientific='Brassica oleracea', common='Cauliflower',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/cauliflower.jpg?itok=8XVweXdc',
                     description='Cauliflower is a cool-season crop.  The plant resembles broccoli with white heads.',
                     water=3, min_temp=40, max_temp=75)
Plant.objects.create(hardiness=[2, 3, 4, 5, 6, 7, 8, 9], photonFluxDensity='Full Sun',
                     space='10-12 inches', toxicity='None', grow_time=112,
                     scientific='Apium graveolens', common='Celery',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/celery_stalks.jpg?itok=cmfi2xA2',
                     description='A plant with a long stringy stalk.',
                     water=1, min_temp=50, max_temp=70)
Plant.objects.create(hardiness=[2, 3, 4, 5, 6, 7, 8, 9], photonFluxDensity='Full Sun, Part Sun',
                     space='18-24 inches', toxicity='None', grow_time=40,
                     scientific='Beta vulgaris', common='Chard',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/chard.jpg?itok=gACQto08',
                     description='A green leafy vegetable with lots of nutrients.',
                     water=4, min_temp=40, max_temp=95)
Plant.objects.create(hardiness=[4, 5, 6, 7, 8], photonFluxDensity='Full Sun',
                     space='4-6 inches apart', toxicity='None', grow_time=80,
                     scientific='Zea mays', common='Corn',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/ears_of_corn.jpg?itok=aDxplSAQ',
                     description='Yellow and starchy.',
                     water=3, min_temp=36, max_temp=112)
Plant.objects.create(hardiness=[4, 5, 6, 7, 8, 9, 10, 11], photonFluxDensity='Full Sun',
                     space='12 inches apart', toxicity='None', grow_time=55,
                     scientific='Cucumis Sativus', common='Cucumber',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/cucumber_plate.jpg?itok=qxFyanxh',
                     description='Long, green and cylindrical.',
                     water=1, min_temp=50, max_temp=95)
Plant.objects.create(hardiness=[4, 5, 6, 7, 8, 9, 10], photonFluxDensity='Full Sun',
                     space='24-30', toxicity='None', grow_time=112,
                     scientific='Solanum melongena', common='Eggplant',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/eggplant.jpg?itok=bker7b8V',
                     description='Egg-shaped glossy purple fruit with white flesh and a meaty texture',
                     water=1, min_temp=50, max_temp=90)
Plant.objects.create(hardiness=[3, 4, 5, 6, 7, 8], photonFluxDensity='Full Sun',
                     space='2-4 inches apart', toxicity='Vampires', grow_time=90,
                     scientific='Allium sativum', common='Garlic',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/garlic.jpg?itok=Bnesgmcu',
                     description='Famous bulb also known as "The Stinking Rose."',
                     water=7, min_temp=20, max_temp=90)
Plant.objects.create(hardiness=[6, 7, 8, 9, 10], photonFluxDensity='Full Sun',
                     space='8-12 inches', toxicity='None', grow_time=56,
                     scientific='Brassica oleracea acephala', common='Kale',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/kale.jpg?itok=2Tee1Onp',
                     description='A hardy, cool-season green that is part of the cabbage family.',
                     water=2, min_temp=20, max_temp=80)
Plant.objects.create(hardiness=[4, 5, 6, 7, 8, 9], photonFluxDensity='Full Sun',
                     space='4 inches apart', toxicity='None', grow_time=14,
                     scientific='Lactuca sativa', common='Lettuce',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/lettuce_rows.jpg?itok=RzCZBJtR',
                     description='Green leafy vegetable commonly used in salads sandwiches and burgers.',
                     water=2, min_temp=40, max_temp=85)
Plant.objects.create(hardiness=[6, 7, 8, 9], photonFluxDensity='Full Sun',
                     space='12-18 inches apart', toxicity='None', grow_time=52,
                     scientific='Abelmoschus esculentus', common='Okra',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/okra.jpg?itok=lIXb02KR',
                     description='Southern plant rich in vitamin A.',
                     water=4, min_temp=70, max_temp=85)
Plant.objects.create(hardiness=[3, 4, 5, 6, 7, 8, 9], photonFluxDensity='Full Sun',
                     space='4-5 inches apart', toxicity='None', grow_time=120,
                     scientific='Allium cepa', common='Onion',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/onions_0.jpg?itok=OvfzUNMt',
                     description='Layered like an ogre, provides great flavor to many dishes.',
                     water=7, min_temp=40, max_temp=100)
Plant.objects.create(hardiness=[2, 3, 4, 5, 6, 7, 8, 9], photonFluxDensity='Full Sun, Part Sun',
                     space='', toxicity='None', grow_time=112,
                     scientific='Pastinaca sativa', common='Parsnip',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/parsnip.jpg?itok=UnalgxM1',
                     description='Hardy, cool season root.',
                     water=4, min_temp=20, max_temp=85)
Plant.objects.create(hardiness=[3, 4, 5, 6, 7, 8, 9, 10, 11], photonFluxDensity='Full Sun, Part Sun',
                     space='2 inches apart', toxicity='None', grow_time=55,
                     scientific='Pisum sativum', common='Peas',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/peas-633948_1280_1.jpg?itok=5UEiYadA',
                     description='Little grean peas in a pod.',
                     water=7, min_temp=45, max_temp=75)
Plant.objects.create(hardiness=[1, 2, 3, 4, 5, 6, 7], photonFluxDensity='Full Sun',
                     space='12 inches apart', toxicity='Do not eat green!', grow_time=70,
                     scientific='Solanum tuberosum', common='Potato',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/ptoatoes.jpg?itok=DjF2aFg7',
                     description='Greatest "vegetable" in the world.  Bake em mash em fry em!',
                     water=1, min_temp=0, max_temp=80)
Plant.objects.create(hardiness=[3, 4, 5, 6, 7, 8, 9], photonFluxDensity='Full Sun',
                     space='4 to 8 feet apart', toxicity='None', grow_time=90,
                     scientific='Cucurbita maxima', common='Pumpkin',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/rowofpumpkins.jpg?itok=uHwX-JXM',
                     description='Behold the Great Pumpkin Charlie Brown',
                     water=3, min_temp=70, max_temp=100)
Plant.objects.create(hardiness=[2, 3, 4, 5, 6, 7, 8, 9, 10], photonFluxDensity='Full Sun',
                     space='12 inches apart', toxicity='None', grow_time=42,
                     scientific='Raphanus sativus', common='Radish',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/photo_2772.jpg?itok=9m2wsZTk',
                     description='Small red garden vegetable that is a little spicy.',
                     water=3, min_temp=35, max_temp=90)
Plant.objects.create(hardiness=[3, 4, 5, 6, 7, 8, 9], photonFluxDensity='Full Sun',
                     space='1 inch apart', toxicity='None', grow_time=42,
                     scientific='Spinacia oleracea', common='Spinach',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/spinach1.jpg?itok=Tht5dGJ_',
                     description='Leafy vegatable that help you fight Bluto.',
                     water=1, min_temp=40, max_temp=70)
Plant.objects.create(hardiness=[2, 3, 4, 5, 6, 7], photonFluxDensity='Full Sun',
                     space='2 to 3 feet apart', toxicity='None', grow_time=65,
                     scientific='Cucurbita', common='Squash',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/zucchini.jpg?itok=LMDzv7Ac',
                     description='There are several varieties including Zucchini.',
                     water=3, min_temp=60, max_temp=100)
Plant.objects.create(hardiness=[9, 10, 10], photonFluxDensity='Full Sun',
                     space='12 to 18 inches apart', toxicity='None', grow_time=120,
                     scientific='Ipomoea batatas', common='Sweet Potato',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/sweet-potatoes.jpg?itok=XodZqhBQ',
                     description='Almost as good as a regular potato.  Needs Very hot temperatures.',
                     water=2, min_temp=70, max_temp=120)
Plant.objects.create(hardiness=[2, 3, 4, 5, 6, 7, 8, 9, 10], photonFluxDensity='Full Sun',
                     space='Needs a cage.  1 per cage.', toxicity='None', grow_time=75,
                     scientific='Lycopersicon esculentum', common='Tomato',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/images/tomatoplant.jpg?itok=0y3zZvMa',
                     description='Red vegetable that is really a fruit.  Mind = Blown.',
                     water=1, min_temp=50, max_temp=110)
Plant.objects.create(hardiness=[3, 4, 5, 6, 7, 8, 9], photonFluxDensity='Full Sun',
                     space='2-4 inches', toxicity='None', grow_time=60,
                     scientific='Brassica rapa Rapifera Group', common='Turnip',
                     ripeURL='http://www.almanac.com/sites/default/files/styles/feature_content_square_220x220/public/image_nodes/planting-growing-turnips.jpg?itok=6SBEA10S',
                     description='Root vegetable grown by parents to torture their children at mealtimes.',
                     water=4, min_temp=40, max_temp=80)
